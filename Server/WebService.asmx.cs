﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace Server
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public string getData(float number)
        {
            return (number * 2).ToString();
        }

        [WebMethod]
        public string getFullName(int id)
        {
            string fullname = "";
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (LocalDB)\MSSQLLocalDB;
                                     AttachDbFilename = |DataDirectory|\Database1.mdf;
                                     Integrated Security = True";

            SqlCommand cmd = new SqlCommand("SELECT FirstName, LastName FROM [Table] WHERE Id = " + id, con);

            con.Open();
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                fullname = dr[0].ToString() + " " + dr[1].ToString();
            }
            con.Close();

            return fullname;
        }

        [WebMethod]
        public string getFullTable()
        {
            string json = "";

            JavaScriptSerializer jss = new JavaScriptSerializer();
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (LocalDB)\MSSQLLocalDB;
                                     AttachDbFilename = |DataDirectory|\Database1.mdf;
                                     Integrated Security = True";

            SqlCommand cmd = new SqlCommand("SELECT * FROM [Table]", con);

            con.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            List<Person> TableData = new List<Person>();
            while (dr.Read())
            {
                TableData.Add(new Person
                {
                    Id = Int32.Parse(dr[0].ToString()),
                    FirstName = dr[1].ToString(),
                    LastName = dr[2].ToString()
                });
                //fullname = dr[0].ToString() + " " + dr[1].ToString();
            }
            json = jss.Serialize(TableData);
            con.Close();

            return json;
        }

        [WebMethod]
        public void addToTable(string firstName, string lastName)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source = (LocalDB)\MSSQLLocalDB;
                                     AttachDbFilename = |DataDirectory|\Database1.mdf;
                                     Integrated Security = True";
            
            SqlCommand cmdMaxId = new SqlCommand("SELECT MAX(Id) FROM [Table]", con);

            int id = -1;
            con.Open();
            SqlDataReader dr = cmdMaxId.ExecuteReader();
            while (dr.Read())
            {
                id = Int32.Parse(dr[0].ToString());
                id++;
            }
            dr.Close();
            SqlCommand cmd = new SqlCommand("INSERT INTO [Table] (Id, FirstName, LastName) VALUES (" + id + ", '" + firstName + "', '" + lastName + "')", con);
            cmd.ExecuteNonQuery();
            con.Close();
        }
        
    }
}
